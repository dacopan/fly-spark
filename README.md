## requirements
- instal scala
- install spark
- sudo pip install numpy
- https://github.com/fommil/netlib-java#linux

## datasets
- https://www.transtats.bts.gov/DL_SelectFields.asp?Table_ID=236&DB_Short_Name=On-Time

## reference
- https://www.codementor.io/jadianes/building-a-recommender-with-apache-spark-python-example-app-part1-du1083qbw
- https://www.codementor.io/jadianes/building-a-web-service-with-apache-spark-flask-example-app-part2-du1083854
- https://github.com/Sally68/Flight-delay-prediction-with-spark/blob/master/Flight%20delay%20prediction%20%20I.ipynb
- https://mapr.com/blog/predicting-breast-cancer-using-apache-spark-machine-learning-logistic-regression/

## otros
- https://www.datacamp.com/community/tutorials/apache-spark-python#gs.yUgdyks

https://www.codementor.io/jadianes/building-a-web-service-with-apache-spark-flask-example-app-part2-du1083854

https://github.com/databricks/learning-spark/blob/master/src/main/scala/com/oreilly/learningsparkexamples/scala/MLlib.scala

https://stackoverflow.com/questions/37303855/predicting-probabilities-of-classes-in-case-of-gradient-boosting-trees-in-spark

https://mapr.com/blog/apache-spark-machine-learning-tutorial/


---------
veras para ejecutar esta asi
es un script de pyhton

entonces debes darle ahi abajo donde dice terminal

2) abres la consola de spark
pyspark


ahi ya puedes ejcutar los comandos entonces ejecutas
desde la primera linea hasta donde esta señalado antes de donde dice TEST

lo q hace es esto
- cargar los datos
- transformar
- procesar
- crear modelo
- entrenar
- hacer el 70/30
- persistir el modelo

3) ya puedes probar una prediccion q quieras
obvio cuando ya acabe de hacer lo q necesita hacer 
viste como se queda ya eserando???

tonces segun yo ahi predice de acuerdo a esos datos
si el vuelo se va retrazar o no

4) salir

5) volver a ejecutar para probar q si se persisio el modelo
es decir solo vas a ejecutar la prediccion directo
y no volver hacer todo el rpoceso de cargar, entrenar

6) importas los imports

7) cargas modelo guardado

## executors
http://localhost:4040/executors/

# probar app
- [vuelo retrazo](http://localhost:5432/vuelo/2017/02/15/19/679/192)
- [vuelo NO_retrazo](http://localhost:5432/vuelo/8/713/5)
- [vuelo NO_retrazo](http://localhost:5432/vuelo/15/853/3)
- [vuelo NO_retrazo](http://localhost:5432/vuelo/6/622/53)

# API
- [aeropuertos/list](http://localhost:5432/aeropuertos/list)
- [aeropuertos/retrazo](http://localhost:5432/aeropuertos/retrazo)
- [aeropuertos/noretrazo](http://localhost:5432/aeropuertos/noretrazo)
- [rutas/retrazo](http://localhost:5432/rutas/retrazo)
- [rutas/noretrazo](http://localhost:5432/rutas/noretrazo)
- [retraso/causa](http://localhost:5432/retraso/causa)


# no retrazo
8 LAR 713
6 BJI 713
8 PIT  1814
8 SFO 8998
# retrazo

16 PIT 1814