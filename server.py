import time, sys, cherrypy, os
from paste.translogger import TransLogger
from app import create_app
from pyspark import SparkContext, SparkConf, SQLContext


def init_spark_context():
    # cargar spark context
    conf = SparkConf().setAppName("uce_vuelos-retrazo")
    sc = SparkContext(conf=conf, pyFiles=['engine.py', 'app.py'])

    return sc


def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"


def run_server(app):
    application_config = {
        '/': {
            'tools.CORS.on': True
        },
    }

    cherrypy.tree.mount(None, '', application_config)

    # Habilitamos acceso WSGI para logging via Paste
    app_logged = TransLogger(app)

    cherrypy.tree.graft(app_logged, '/')

    # Configuraciones para el servidor web
    cherrypy.config.update({
        'engine.autoreload.on': True,
        'log.screen': True,
        'server.socket_port': 5432,
        'server.socket_host': '0.0.0.0',
        'tools.CORS.on': True
    })

    cherrypy.tools.CORS = cherrypy.Tool('before_handler', CORS)
    cherrypy.response.headers['Content-Type'] = 'application/json'

    # Iniciamos el servidor web
    cherrypy.engine.start()
    # app.run()
    cherrypy.engine.block()


if __name__ == "__main__":
    # Inciiamos el spark context y cargamos las librerias
    sc = init_spark_context()
    sqlContext = SQLContext(sc)

    work_path = '/opt/spark/hdfs/fly-spark'
    app = create_app(sc, sqlContext, work_path)

    # llamaos a la funcion q inica el servidor web
    run_server(app)
