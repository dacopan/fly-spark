import datetime

import cherrypy
from flask import Blueprint

main = Blueprint('main', __name__)

import json
from engine import VuelosEngine
from flask import Flask, request
from pyspark.mllib.linalg import Vectors
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@main.after_request
def apply_caching(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response


@main.route("/vuelo/<int:hora>/<int:distancia>/<int:originIndex>", methods=["GET"])
def top_ratings(hora, distancia, originIndex):
    logger.debug("Predecir retrazo h:%d %d %d", hora, distancia, originIndex)
    # weekday = datetime.date(year, mes, dia).weekday()
    feature = Vectors.dense(float(hora), float(distancia), 1.0)
    prediccion = engine.predecir_vuelo((originIndex + 2), feature)
    return json.dumps(prediccion)


@main.route("/johy", methods=["GET"])
def johy():
    weekday = datetime.date(2017, 11, 11).weekday()
    return json.dumps(weekday)


@main.route("/aeropuertos/list", methods=["GET"])
def aeropuertos():
    logger.debug("aeropuertos/list")
    top_aeropuertos = engine.get_origin()
    return top_aeropuertos


@main.route("/aeropuertos/retrazo", methods=["GET"])
def aeropuertos_retrazo():
    logger.debug("aeropuertos/retrazo")
    top_aeropuertos = engine.get_aeropuertos_retrazo()
    return top_aeropuertos


@main.route("/aeropuertos/noretrazo", methods=["GET"])
def aeropuertos_noretrazo():
    logger.debug("aeropuertos/noretrazo")
    top_aeropuertos = engine.get_aeropuertos_no_retrazo()
    return top_aeropuertos


@main.route("/rutas/noretrazo", methods=["GET"])
def rutas_noretrazo():
    logger.debug("rutas/noretrazo")
    top_origen = engine.get_rutas_no_retrazo()
    return top_origen


@main.route("/rutas/retrazo", methods=["GET"])
def rutas_retrazo():
    logger.debug("rutas/retrazo")
    top_origen = engine.get_rutas_retrazo()
    return top_origen


@main.route("/retraso/causa", methods=["GET"])
def top_causas():
    logger.debug("retraso/causa")
    top_aeropuertos = engine.get_cause()
    return top_aeropuertos


def create_app(spark_context, sql_context, dataset_path):
    global engine

    engine = VuelosEngine(spark_context, sql_context, dataset_path)

    app = Flask(__name__)
    app.register_blueprint(main)
    return app
