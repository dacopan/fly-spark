var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        aeroRetrazoLoad: true,
        aeroNoRetrazoLoad: true,
        rutaRetrazoLoad: true,
        rutaNoRetrazoLoad: true,
        retrazoCausaLoad: true,
        aeropuesrtosLoad: true,
        aeroRetrazo: [],
        aeroNoRetrazo: [],
        rutaRetrazo: [],
        rutaNoRetrazo: [],
        retrazoCausa: [],
        aeropuertos: [],
        prediccion: ''

    },
    methods: {
        predecir: function (event) {
            // `this` inside methods points to the Vue instance
            // `event` is the native DOM event
            app.prediccion = 'cargando...';
            var hora = $('#hora').val().split(':')[0];
            $.ajax({
                url: 'http://localhost:5432/vuelo/' + hora + '/' + $('#distancia').val() + '/' + $('#origen').val()
            }).then(function (data) {
                if (parseInt(data) === 1) {
                    app.prediccion = 'Ohh! Nooo, parece q tu vuelo se retrazará';
                } else {
                    app.prediccion = 'En hora buena, parece q tu vuelo no se retrazará';
                }
            });
        }
    }
});


$(function () {
    console.log('johy');

    load_list_aeropuertos();

    $('#input_starttime').pickatime({
        twelvehour: false
    });


    load_aeropuertos(true);
    load_aeropuertos(false);
    load_ruta(true);
    load_ruta(false);
    retrazo_causa();
});

function load_list_aeropuertos() {
    $.ajax({
        url: 'http://localhost:5432/aeropuertos/list'
    }).then(function (data) {
        data = $.parseJSON(data);
        data = data.sort(function (a, b) {
            //return a.attributes.OBJECTID - B.attributes.OBJECTID;
            if (a.originIndex == b.originIndex)
                return 0;
            if (a.originIndex < b.originIndex)
                return -1;
            if (a.originIndex > b.originIndex)
                return 1;
        });
        var l = data.length;
        $.each(data, function (i, item) {
            app.aeropuertos.push(item);
            if (!--l) {
                setTimeout(function () {
                    $('.mdb-select').material_select();
                }, 500)
            }
        });

        app.aeropuesrtosLoad = false;
    });
}

function load_aeropuertos(retrazo) {
    $.ajax({
        url: 'http://localhost:5432/aeropuertos/' + (retrazo ? 'retrazo' : 'noretrazo')
    }).then(function (data) {
        data = $.parseJSON(data);
        var c = retrazo ? app.aeroRetrazo : app.aeroNoRetrazo;
        $.each(data, function (i, item) {
            c.push(item);
        });
        if (retrazo) {
            app.aeroRetrazoLoad = false;
        } else {
            app.aeroNoRetrazoLoad = false;
        }
    });
}

function load_ruta(retrazo) {
    $.ajax({
        url: 'http://localhost:5432/rutas/' + (retrazo ? 'retrazo' : 'noretrazo')
    }).then(function (data) {
        data = $.parseJSON(data);
        var c = retrazo ? app.rutaRetrazo : app.rutaNoRetrazo;
        $.each(data, function (i, item) {
            c.push(item);
        });
        if (retrazo) {
            app.rutaRetrazoLoad = false;
        } else {
            app.rutaNoRetrazoLoad = false;
        }
    });
}


function retrazo_causa() {
    $.ajax({
        url: 'http://localhost:5432/retraso/causa'
    }).then(function (data) {
        data = $.parseJSON(data);
        $.each(data, function (i, item) {
            app.retrazoCausa.push(item);
        });
        app.retrazoCausaLoad = false;
    });
}