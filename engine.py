import logging
import os

from pyspark import Row
from pyspark.mllib.classification import LogisticRegressionModel
from pyspark.mllib.linalg import Vectors
from pyspark.sql.functions import udf
from pyspark.sql.types import IntegerType
from pyspark.ml.feature import OneHotEncoder, StringIndexer
from pyspark.ml.feature import VectorAssembler
import pandas as pd

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def encerar(r):
    if r == '':
        return r
    else:
        return r


def parse(r):
    try:
        x = Row(Year=int(encerar(r[0])),
                Month=int(encerar(r[1])),
                DayofMonth=int(encerar(r[2])),
                DayOfWeek=int(encerar(r[3])),
                UniqueCarrier=r[4],
                Origin=r[6],
                Dest=r[7],
                CRSDepTime=int(encerar(r[8])),
                DepTime=int(float(encerar(r[9]))),
                DepDelay=int(float(encerar(r[10]))),
                CRSArrTime=int(encerar(r[11])),
                ArrTime=int(float(encerar(r[12]))),
                Distance=int(float(encerar(r[13]))),
                CarrierDelay=int(float(encerar(r[14]))),
                WeatherDelay=int(float(encerar(r[15]))),
                NASDelay=int(float(encerar(r[16]))),
                SecurityDelay=int(float(encerar(r[17]))),
                LateAircraftDelay=int(float(encerar(r[18]))))
    except:
        x = None
    return x


class VuelosEngine:
    """Motor de prediccion de vuelos retrazados
    """

    def __load_model(self):
        """No necesario porque el modelo ya es persistente
        """
        datasets_path = os.path.join(self.work_path, 'datasets')
        # small_ratings_file = os.path.join(datasets_path, 'airline_delay_2017-02-small.csv')
        small_ratings_file = os.path.join(datasets_path, 'airline_delay_2017-02.csv')
        small_ratings_raw_data = self.sc.textFile(small_ratings_file)
        small_ratings_raw_data_header = small_ratings_raw_data.take(1)[0]

        self.small_ratings_data = small_ratings_raw_data.filter(lambda line: line != small_ratings_raw_data_header) \
            .map(lambda line: line.split(",")).map(lambda r: parse(r)).filter(lambda r: r is not None).cache()

        airline_df = self.sqlContext.createDataFrame(self.small_ratings_data)

        airline_df = airline_df.withColumn('DepDelayed', airline_df['DepDelay'] > 15)

        # define hour function to obtain hour of day
        def hour_ex(x):
            h = int(str(int(x)).zfill(4)[:2])
            return h

        # register as a UDF
        f = udf(hour_ex, IntegerType())

        # CRSDepTime: scheduled departure time (local, hhmm)
        airline_df = airline_df.withColumn('hour', f(airline_df.CRSDepTime))

        self.airline_df = self.__transform_df(airline_df)
        self.airline_df.registerTempTable("df_out")

    def __transform_df(self, df_ORG):
        df_model = df_ORG
        stringIndexer1 = StringIndexer(inputCol="Origin", outputCol="originIndex")
        model_stringIndexer = stringIndexer1.fit(df_model)
        indexedOrigin = model_stringIndexer.transform(df_model)
        encoder1 = OneHotEncoder(dropLast=False, inputCol="originIndex", outputCol="originVec")
        df_model = encoder1.transform(indexedOrigin)

        # We use labeled point to make local vectors associated with a label/response.
        # In MLlib, labeled points are used in supervised learning algorithms and they are stored as doubles.
        # For binary classification, a label should be either 0 (negative) or 1 (positive).

        assembler = VectorAssembler(
            inputCols=['hour', 'Distance', 'originVec'],
            outputCol="features")
        output = assembler.transform(df_model)
        output.take(1)

        from pyspark.mllib.util import MLUtils

        output = MLUtils.convertVectorColumnsFromML(output)
        return output

    def predecir_vuelo(self, originIndex, features):
        """Gets predictions for a given (userID, movieID) formatted RDD
        Returns: an RDD with format (movieTitle, movieRating, numRatings)
        """
        feature = Vectors.sparse(297, [0, 1, originIndex], features)
        print("engine intentar predecir ->")
        print(feature)
        prediccion = self.sameModel.predict(feature)
        return prediccion

    def get_aeropuertos_retrazo(self):
        groupedDelay = self.sqlContext.sql("SELECT Origin, originIndex, count(*) traffic, avg(DepDelay) delay \
                                        FROM df_out \
                                        GROUP BY Origin, originIndex HAVING traffic>10 ORDER BY delay DESC LIMIT 10").cache()

        # df_origin = groupedDelay.toPandas()
        return groupedDelay.toPandas().to_json(orient='records')

    def get_aeropuertos_no_retrazo(self):
        groupedDelay = self.sqlContext.sql("SELECT Origin, originIndex, count(*) traffic, avg(DepDelay) delay \
                                        FROM df_out \
                                        GROUP BY Origin, originIndex HAVING traffic>10 ORDER BY delay LIMIT 10").cache()

        # df_origin = groupedDelay.toPandas()
        return groupedDelay.toPandas().to_json(orient='records')

    def get_rutas_retrazo(self):
        groupedDelay = self.sqlContext.sql("SELECT Origin, originIndex, Dest, count(*) traffic, avg(Distance) avgDist,\
                                avg(DepDelay) delay\
                                FROM df_out \
                                GROUP BY Origin, originIndex, Dest HAVING traffic>10 ORDER BY delay DESC LIMIT 10").cache()

        # df_origin = groupedDelay.toPandas()
        return groupedDelay.toPandas().to_json(orient='records')

    def get_rutas_no_retrazo(self):
        groupedDelay = self.sqlContext.sql("SELECT Origin, originIndex, Dest, count(*) traffic, avg(Distance) avgDist,\
                                avg(DepDelay) delay\
                                FROM df_out \
                                GROUP BY Origin, originIndex, Dest HAVING traffic>10 ORDER BY delay LIMIT 10").cache()

        # df_origin = groupedDelay.toPandas()
        return groupedDelay.toPandas().to_json(orient='records')

    def get_cause(self):
        cause_delay = self.sqlContext.sql("SELECT sum(WeatherDelay) Weather,sum(NASDelay) NAS,sum(SecurityDelay) Security,sum(LateAircraftDelay) lateAircraft,sum(CarrierDelay) Carrier\
                                      FROM df_out ").cache()
        return cause_delay.toPandas().to_json(orient='records')

    def get_origin(self):
        cause_delay = self.sqlContext.sql("SELECT distinct Origin, originIndex, avg(DepDelay) delay\
                                FROM df_out \
                                GROUP BY Origin, originIndex ORDER BY delay").cache()
        return cause_delay.toPandas().to_json(orient='records')

    def __init__(self, sc, sqlContext, work_path):
        """ Inicializa el motor dado un contexto de Spark context and el path donde esta alamacenado el modelo
        """

        logger.info("Iniciando Motor: ")

        self.sc = sc
        self.sqlContext = sqlContext

        # cargamos el modelo previamente guardado
        logger.info("Loading model...")

        model_path = os.path.join(work_path, 'models', 'fly_model')
        self.work_path = work_path
        try:
            os.remove(os.path.join('.', 'metastore_db', 'db.lck'))
            os.remove(os.path.join('.', 'metastore_db', 'dbex.lck'))
        except Exception:
            print(Exception)

        # load model
        self.sameModel = LogisticRegressionModel.load(sc, model_path)
        self.__load_model()
