# coding=utf-8
import os
import sys

from pyspark.sql import SQLContext
from pyspark.sql.types import *
from pyspark.sql import Row
from pyspark.mllib.regression import LabeledPoint
from pyspark.sql.functions import udf
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.param import Param, Params
from pyspark.mllib.classification import LogisticRegressionWithLBFGS, LogisticRegressionModel
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.stat import Statistics
from pyspark.ml.feature import OneHotEncoder, StringIndexer
from pyspark.mllib.linalg import Vectors
from pyspark.ml.feature import VectorAssembler
import numpy as np
import shutil

"""
SCRIPT DE INICIALIZACION
- carga dataset
- crea dataframe
- crea modelo de regresion
- entrena modelo
- prueba modelo
- persiste el modelo
"""

"""
cargamos los datasets
"""

from pyspark import SparkContext

# sc = SparkContext()
# sqlContext = SQLContext(sc)
work_path = '/opt/spark/hdfs/fly-spark'
datasets_path = os.path.join(work_path, 'datasets')
# small_ratings_file = os.path.join(datasets_path, 'airline_delay_2017-02-small.csv')
file_02 = os.path.join(datasets_path, 'airline_delay_2017-02.csv')
# file_01 = os.path.join(datasets_path, 'airline_delay_2017-01.csv')

# We need to filter out the header, included in each
# small_ratings_raw_data = sc.textFile(file_02).union(sc.textFile(file_01))
small_ratings_raw_data = sc.textFile(file_02)
small_ratings_raw_data_header = small_ratings_raw_data.take(1)[0]

print(">>> Results: ")

small_ratings_raw_data.take(3)
"""
Creating the Dataframe from RDD
"""


def encerar(r):
    if r == '':
        return r
    else:
        return r


def parse(r):
    try:
        x = Row(Year=int(encerar(r[0])),
                Month=int(encerar(r[1])),
                DayofMonth=int(encerar(r[2])),
                DayOfWeek=int(encerar(r[3])),
                UniqueCarrier=r[4],
                Origin=r[6],
                Dest=r[7],
                CRSDepTime=int(encerar(r[8])),
                DepTime=int(float(encerar(r[9]))),
                DepDelay=int(float(encerar(r[10]))),
                CRSArrTime=int(encerar(r[11])),
                ArrTime=int(float(encerar(r[12]))),
                Distance=int(float(encerar(r[13]))),
                CarrierDelay=int(float(encerar(r[14]))),
                WeatherDelay=int(float(encerar(r[15]))),
                NASDelay=int(float(encerar(r[16]))),
                SecurityDelay=int(float(encerar(r[17]))),
                LateAircraftDelay=int(float(encerar(r[18]))))
    except:
        x = None
    return x


# Now we can parse the raw data into a new RDD.
small_ratings_data = small_ratings_raw_data.filter(lambda line: line != small_ratings_raw_data_header) \
    .map(lambda line: line.split(",")).map(lambda r: parse(r)).filter(lambda r: r is not None).cache()

# test
small_ratings_data.take(3)

# Creating the Dataframe
airline_df = sqlContext.createDataFrame(small_ratings_data)

# add a new column to our data frame, DepDelayed, a binary variable:
# True, for flights that have > 15 minutes of delay False, for flights that have <= 15 minutes of delay
airline_df = airline_df.withColumn('DepDelayed', airline_df['DepDelay'] > 15)


# define hour function to obtain hour of day
def hour_ex(x):
    h = int(str(int(x)).zfill(4)[:2])
    return h


# register as a UDF
f = udf(hour_ex, IntegerType())

# CRSDepTime: scheduled departure time (local, hhmm)
airline_df = airline_df.withColumn('hour', f(airline_df.CRSDepTime))
airline_df.registerTempTable("airlineDF")

"""
PARTE II
fly delay sample data
"""

"""
Modeling: Logistic Regression
"""
# Feature selection
df_model = airline_df
stringIndexer1 = StringIndexer(inputCol="Origin", outputCol="originIndex")
model_stringIndexer = stringIndexer1.fit(df_model)
indexedOrigin = model_stringIndexer.transform(df_model)
encoder1 = OneHotEncoder(dropLast=False, inputCol="originIndex", outputCol="originVec")
df_model = encoder1.transform(indexedOrigin)

# We use labeled point to make local vectors associated with a label/response.
# In MLlib, labeled points are used in supervised learning algorithms and they are stored as doubles.
# For binary classification, a label should be either 0 (negative) or 1 (positive).

assembler = VectorAssembler(
    inputCols=['hour', 'Distance', 'originVec'],
    outputCol="features")
output = assembler.transform(df_model)
output.take(1)
output.registerTempTable("df_out")

from pyspark.mllib.util import MLUtils

output = MLUtils.convertVectorColumnsFromML(output)

airlineRDD = output.rdd.map(lambda row: LabeledPoint([0, 1][row['DepDelayed']], row['features']))
airlineRDD.take(1)

#  DIVIDIMOS EL DATASET en datos de entrenar y datos para prueba 70/30
trainRDD, testRDD = airlineRDD.randomSplit([0.7, 0.3])

# Build the model
model = LogisticRegressionWithLBFGS.train(trainRDD)

"""
EVALUAR MODELO
"""
# Evaluating the model on testing data
labelsAndPreds = testRDD.map(lambda p: (p.label, model.predict(p.features)))


def conf(r):
    if r[0] == r[1] == 1: x = 'TP'
    if r[0] == r[1] == 0: x = 'TN'
    if r[0] == 1 and r[1] == 0: x = 'FN'
    if r[0] == 0 and r[1] == 1: x = 'FP'
    return (x)


acc1 = labelsAndPreds.map(lambda (v, p): ((v, p), 1)).reduceByKey(lambda a, b: a + b).take(5)
acc = [(conf(x[0]), x[1]) for x in acc1]

TP = TN = FP = FN = 0.0
for x in acc:
    if x[0] == 'TP': TP = x[1]
    if x[0] == 'TN': TN = x[1]
    if x[0] == 'FP': FP = x[1]
    if x[0] == 'FN': FN = x[1]

# sf
eps = sys.float_info.epsilon
Accuracy = (TP + TN) / (TP + TN + FP + FN + eps)
print "Model Accuracy for JFK: %1.2f %%" % (Accuracy * 100)

trainErr = labelsAndPreds.filter(lambda lp: lp[0] != lp[1]).count() / float(output.count())
print("Training Error = " + str(trainErr))


def eval_metrics(lbl_pred):
    tp = float(lbl_pred.filter(lambda lp: lp[0] == 1.0 and lp[1] == 1.0).count())
    tn = float(lbl_pred.filter(lambda lp: lp[0] == 0.0 and lp[1] == 0.0).count())
    fp = float(lbl_pred.filter(lambda lp: lp[0] == 1.0 and lp[1] == 0.0).count())
    fn = float(lbl_pred.filter(lambda lp: lp[0] == 0.0 and lp[1] == 1.0).count())
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    F_measure = 2 * precision * recall / (precision + recall)
    accuracy = (tp + tn) / (tp + tn + fp + fn)
    return ([tp, tn, fp, fn], [precision, recall, F_measure, accuracy])


metrics = eval_metrics(labelsAndPreds)

print('Precision : %.2f' % round(metrics[1][0], 2))
print('Recall : %.2f' % round(metrics[1][1], 2))
print('F1 : %.2f' % round(metrics[1][2], 2))
print('Accuracy : %.2f' % round(metrics[1][3], 2))

# obtener vuelos retrazados
labelsAndPreds2 = testRDD.map(lambda p: (p.label, model.predict(p.features), p.features))
labelsAndPreds2.filter(lambda lp: lp[0] == lp[1] == 1).take(20)

"""
PERSISTIR MODELO
"""
model_path = os.path.join(work_path, 'models', 'fly_model')

shutil.rmtree(model_path, ignore_errors=True)

model.save(sc, model_path)
sameModel = LogisticRegressionModel.load(sc, model_path)

# EJECUTAR TEST (mas abajo)
# SALIR
quit()

"""
LOAD MODELO
"""
import os
import sys

from pyspark.sql import SQLContext
from pyspark.sql.types import *
from pyspark.sql import Row
from pyspark.mllib.regression import LabeledPoint
from pyspark.sql.functions import udf
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.param import Param, Params
from pyspark.mllib.classification import LogisticRegressionWithLBFGS, LogisticRegressionModel
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.stat import Statistics
from pyspark.ml.feature import OneHotEncoder, StringIndexer
from pyspark.mllib.linalg import Vectors
from pyspark.ml.feature import VectorAssembler
import numpy as np
import shutil

work_path = '/opt/spark/hdfs/fly-spark'
model_path = os.path.join(work_path, 'models', 'fly_model')

os.remove(os.path.join('.', 'metastore_db', 'db.lck'))
os.remove(os.path.join('.', 'metastore_db', 'dbex.lck'))

sameModel = LogisticRegressionModel.load(sc, model_path)

"""
TEST
"""
# predict RETRAZO VERDADERO
# ['Ano', 'Mes', 'DiasMes', 'DiasSemana', 'Hora', 'Distancia', 'OrigenVec'],
feature = Vectors.sparse(302, [0, 1, 191], [19.0, 679.0, 1.0])
sameModel.predict(feature)

# predict cero vuelo no retraza
feature = Vectors.sparse(302, [0, 1, 27], [19.0, 1450.0, 1.0])
sameModel.predict(feature)

"""
ESTADISTICAS
"""
small_ratings_raw_data.count()
output.count()
