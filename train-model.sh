#!/usr/bin/env bash

SPARK_HOME=/opt/spark
$SPARK_HOME/bin/spark-submit --master local enki-fly-starter.py


$SPARK_HOME/bin/spark-submit --master local --total-executor-cores 14 --executor-memory 6g server.py
